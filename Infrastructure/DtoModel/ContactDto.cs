﻿using System;

namespace Infrastructure.DtoModel
{
    public class ContactDto
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }
        public string TelNo { get; set; }
        
    }
}