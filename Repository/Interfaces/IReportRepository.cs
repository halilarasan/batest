﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure.Entity;

namespace Repository.Interfaces
{
    public interface IReportRepository
    {
        public Task<Guid> CreateReport();
        public Task Calculate(Guid reportId);
        public Task<List<Report>> GetReportList();
        public Task<List<ReportDetail>> GetReportDetailList(Guid reportId);
    }
}