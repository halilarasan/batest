using System;

namespace Infrastructure.DtoModel
{
    public class ReportDetailDto
    {
        public Guid ReportDetailId { get; set; }
        public string Location { get; set; }
        public int PersonCount { get; set; }
        public int TelNoCount { get; set; }
        public Guid ReportId { get; set; }
    }
}