using System;
using Infrastructure.Entity;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using Shouldly;
using Xunit;

namespace UnitTest
{
    public class PersonRepositoryTests
    {
        private static string MEMORY_DB_NAME = "BaTestDB";
        [Fact]
        public async void Save_Should_Save_Person_And_Should_Return_All_Count_As_Two()
        {
            var person1 = new Person
            {
                Name = "Test Name 1",
                Surname = "Test Surname 1",
                Company = "Test Company 1"
            };
            var person2 = new Person
            {
                Name = "Test Name 2",
                Surname = "Test Surname 2",
                Company = "Test Company 2"
            };
            var options = new DbContextOptionsBuilder<BaTestDbContext>()
                .UseInMemoryDatabase(MEMORY_DB_NAME)
                .Options;

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                await repository.CreatePerson(person1);
                await repository.CreatePerson(person2);
                await context.SaveChangesAsync();
            }

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                var list = await repository.GetAllPersons();
                list.Count.ShouldBe(2);
            }
        }
        
        [Fact]
        public async void Delete_Should_Delete_The_Person_And_Should_Return_All_Count_As_One()
        {
            var person1 = new Person
            {
                Name = "Test Name 1",
                Surname = "Test Surname 1",
                Company = "Test Company 1"
            };
            var person2 = new Person
            {
                Name = "Test Name 2",
                Surname = "Test Surname 2",
                Company = "Test Company 2"
            };
            var options = new DbContextOptionsBuilder<BaTestDbContext>()
                .UseInMemoryDatabase(MEMORY_DB_NAME)
                .Options;

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                await repository.CreatePerson(person1);
                await repository.CreatePerson(person2);
                await context.SaveChangesAsync();
            }
            
            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                await repository.DeletePerson(person1.Uuid);
                await context.SaveChangesAsync();
            }

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                var list = await repository.GetAllPersons();
                list.Count.ShouldBe(1);
            }
        }
        
        [Fact]
        public async void Save_Should_Save_Contact_And_Should_Return_All_Count_As_Two()
        {
            var person1 = new Person
            {
                Name = "Test Name 1",
                Surname = "Test Surname 1",
                Company = "Test Company 1"
            };
            var contact1 = new Contact
            {
                TelNo = "test tel no 1",
                Email = "test email 1",
                Location = "test location 1"
            };
            var contact2 = new Contact
            {
                TelNo = "test tel no 2",
                Email = "test email 2",
                Location = "test location 2"
            };
            var options = new DbContextOptionsBuilder<BaTestDbContext>()
                .UseInMemoryDatabase(MEMORY_DB_NAME)
                .Options;

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                await repository.CreatePerson(person1);
                await context.SaveChangesAsync();
                
                contact1.PersonId = person1.Uuid;
                contact2.PersonId = person1.Uuid;
                await repository.AddContact(contact1);
                await repository.AddContact(contact2);
                await context.SaveChangesAsync();
            }

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                var list = await repository.GetContacts(person1.Uuid);
                list.Count.ShouldBe(2);
            }
        }
        
        [Fact]
        public async void Delete_Should_Delete_The_Contact_And_Should_Return_All_Count_As_One()
        {
            var person1 = new Person
            {
                Name = "Test Name 1",
                Surname = "Test Surname 1",
                Company = "Test Company 1"
            };
            var contact1 = new Contact
            {
                TelNo = "test tel no 1",
                Email = "test email 1",
                Location = "test location 1"
            };
            var contact2 = new Contact
            {
                TelNo = "test tel no 2",
                Email = "test email 2",
                Location = "test location 2"
            };
            var options = new DbContextOptionsBuilder<BaTestDbContext>()
                .UseInMemoryDatabase(MEMORY_DB_NAME)
                .Options;

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                await repository.CreatePerson(person1);
                await context.SaveChangesAsync();
                
                contact1.PersonId = person1.Uuid;
                contact2.PersonId = person1.Uuid;
                await repository.AddContact(contact1);
                await repository.AddContact(contact2);
                await context.SaveChangesAsync();

                await repository.RemoveContact(person1.Uuid, contact1.Uuid);
            }

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new PersonRepository(context);
                var list = await repository.GetContacts(person1.Uuid);
                list.Count.ShouldBe(1);
            }
        }
    }
}