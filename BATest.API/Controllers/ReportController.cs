﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.States;
using Infrastructure.DtoModel;
using Infrastructure.Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;
using Repository.Repositories;

namespace BATest.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportController : ControllerBase
    {
        private readonly ILogger<ReportController> _logger;
        private readonly IBackgroundJobClient _hangFireClient;
        private readonly IReportRepository _reportRepository;
        public ReportController(ILogger<ReportController> logger,IBackgroundJobClient hangFireClient,IReportRepository reportRepository)
        {
            _hangFireClient = hangFireClient;
            _reportRepository = reportRepository;
            _logger = logger;
        }

        [HttpPost("createreport")]
        public async Task<bool> CreateReport()
        {
            var reportId = await _reportRepository.CreateReport();

            _hangFireClient.Enqueue(() => _reportRepository.Calculate(reportId));
            //EnqueuedState queueState = new EnqueuedState("report");
            //_hangFireClient.Create<ReportRepository>(w => w.Calculate(reportId),queueState);
            
            return true;
        }

        [HttpGet("getreportlist")]
        public async Task<IEnumerable<ReportDto>> GetReportList()
        {
            IEnumerable<ReportDto> result = new List<ReportDto>();
            var reportList = await _reportRepository.GetReportList();
            if (reportList != null && reportList.Count > 0)
            {
                result = reportList.Select(s => new ReportDto
                {
                    ReportId = s.Uuid,
                    ReportDate = s.ReportDate,
                    ReportState = s.ReportState
                });
            }
            return result;
        }

        [HttpGet("getreportdetaillist")]
        public async Task<IEnumerable<ReportDetailDto>> GetReportDetailList(string reportId)
        {
            IEnumerable<ReportDetailDto> result = new List<ReportDetailDto>();
            Guid rId = new Guid(reportId);
            var reportDetailList = await _reportRepository.GetReportDetailList(rId);
            if (reportDetailList != null && reportDetailList.Count > 0)
            {
                result = reportDetailList.Select(s => new ReportDetailDto
                {
                    Location = s.Location,
                    PersonCount = s.PersonCount,
                    TelNoCount = s.TelNoCount,
                    ReportId = s.ReportId,
                    ReportDetailId = s.Uuid
                });
            }
            return result;
        }
    }
}