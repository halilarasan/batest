﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.DtoModel;
using Infrastructure.Entity;
using Infrastructure.Utilities;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using Z.EntityFramework.Plus;

namespace Repository.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly BaTestDbContext _dbc;

        public PersonRepository(BaTestDbContext dbc)
        {
            _dbc = dbc;
        }

        public async Task<List<Person>> GetAllPersons()
        {
            return await _dbc.Person.Select( s=> new Person
                {
                    Uuid = s.Uuid,
                    Name = s.Name,
                    Surname = s.Surname,
                    Company = s.Company
                }).ToListAsync();
        }

        public async Task<bool> CreatePerson(Person person)
        {
            await _dbc.Person.AddAsync(person);
            await _dbc.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeletePerson(Guid personId)
        {
            var person = _dbc.Person.First(w => w.Uuid == personId);
            _dbc.Person.Remove(person);
            await _dbc.SaveChangesAsync();
            return true;
        }

        public async Task<bool> AddContact(Contact contact)
        {
            await _dbc.Contact.AddAsync(contact);
            await _dbc.SaveChangesAsync();
            return true;
        }

        public async Task<bool> RemoveContact(Guid personId,Guid contactId)
        {
            var contact = await _dbc.Contact.FindAsync(contactId);
            _dbc.Contact.Remove(contact);
            await _dbc.SaveChangesAsync();
            return true;
        }

        public async Task<List<Contact>> GetContacts(Guid personId)
        {
            return await _dbc.Contact.Where(w=>w.PersonId == personId).ToListAsync();
        }
    }
}