﻿using System;
using System.ComponentModel.DataAnnotations;
using Infrastructure.Enum;

namespace Infrastructure.Entity
{
    public class Contact : BaseEntity
    {
        public string Email { get; set; }
        public string Location { get; set; }
        public string TelNo { get; set; }
        
        public Person Person { get; set; }
        public Guid PersonId { get; set; }
    }
}