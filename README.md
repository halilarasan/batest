### Projenin Çalıştırılması
Projede Postgresql, Dotnet Core 3.1 kullanılmıştır.

1- ConnectionString ayarlaması \BATest.API klasöründe, "appsettings.json" dosyası içerisindeki "BATestDBConnection" alanı değiştirilerek yapılmalı. 

2- \BATest klasörüne gidilerek sırasıyla aşağıdaki komutlar yazılmalı.
	

    dotnet clean
    dotnet build
3- \BATest.API klasörüne gidilerek sırasıyla aşağıdaki komutlar yazılmalı.

    dotnet clean
    dotnet build
4- \BATest.API klasöründe aşağıdaki komut ile migration çalıştırılmalı.

    dotnet ef database update
5- \BATest.API\bin\Debug\netcoreapp3.1 klasörüne giderek API uygulaması aşağıdaki komut ile çalıştırılmalı.

    dotnet BATest.API.dll
6- \BATest\bin\Debug\netcoreapp3.1 klasörüne giderek kullanıcı arayüz uygulaması aşağıdaki komut ile çalıştırılmalı.

    dotnet BATest.dll



> **Not 1:** Kullanıcı arayüzüne  https://localhost:5001/ adresinden ulaşabilirsiniz.

> **Not 2:** Background Job incelemesi için http://localhost:6001/hangfire adresine gidebilirsiniz.

> **Not 3:** API yapısı inceleme ve test işlemleri için http://localhost:6001/swagger adresine gidebilirsiniz.
