﻿using System;
using Infrastructure.Enum;

namespace Infrastructure.Entity
{
    public class ReportDetail : BaseEntity
    {
        public string Location { get; set; }
        public int PersonCount { get; set; }
        public int TelNoCount { get; set; }
        
        public Report Report { get; set; }
        public Guid ReportId { get; set; }
    }
}