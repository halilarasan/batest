﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.DtoModel;
using Infrastructure.Entity;

namespace Repository.Interfaces
{
    public interface IPersonRepository
    {
        public Task<List<Person>> GetAllPersons();
        public Task<bool> CreatePerson(Person person);
        public Task<bool> DeletePerson(Guid personId);
        public Task<bool> AddContact(Contact contact);
        public Task<bool> RemoveContact(Guid personId,Guid contactId);
        public Task<List<Contact>> GetContacts(Guid personId);
    }
}