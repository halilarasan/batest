using System;
using Infrastructure.Entity;
using Infrastructure.Enum;
using Microsoft.EntityFrameworkCore;
using Repository.Repositories;
using Shouldly;
using Xunit;

namespace UnitTest
{
    public class ReportRepositoryTests
    {
        private static string MEMORY_DB_NAME = "BaTestDB";
        [Fact]
        public async void Save_Should_Save_Report_And_Should_Return_All_Count_As_Two()
        {
            var options = new DbContextOptionsBuilder<BaTestDbContext>()
                .UseInMemoryDatabase(MEMORY_DB_NAME)
                .Options;

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new ReportRepository(context);
                await repository.CreateReport();
                await repository.CreateReport();
                await context.SaveChangesAsync();
            }

            await using (var context = new BaTestDbContext(options))
            {
                var repository = new ReportRepository(context);
                var list = await repository.GetReportList();
                list.Count.ShouldBe(2);
            }
        }
    }
}