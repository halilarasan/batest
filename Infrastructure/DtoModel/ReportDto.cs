﻿using System;
using Infrastructure.Enum;

namespace Infrastructure.DtoModel
{
    public class ReportDto
    {
        public Guid ReportId { get; set; }
        public DateTime ReportDate { get; set; }
        public ReportState ReportState { get; set; }
    }
}