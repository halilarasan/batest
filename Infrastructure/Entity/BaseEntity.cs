﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.Entity
{
    public class BaseEntity
    {
        [Key]
        public Guid Uuid { get; set; }
    }
}