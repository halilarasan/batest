using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BATest.Utilities
{
    public static class StringExtension
    {
        public static string GetQueryString(this object obj) {
            var properties = from p in obj.GetType().GetProperties()
                where p.GetValue(obj, null) != null
                select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null)?.ToString()?.ToLowerInvariant());

            return string.Join("&", properties.ToArray());
        }
    }
}