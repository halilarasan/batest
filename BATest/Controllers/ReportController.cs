﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BATest.Models;
using BATest.Utilities;
using Infrastructure;
using Infrastructure.DtoModel;
using Infrastructure.Entity;
using Repository.Interfaces;

namespace BATest.Controllers
{
    public class ReportController : Controller
    {
        private readonly ILogger<ReportController> _logger;
        public ReportController(ILogger<ReportController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> CreateReport()
        {
            var result = await Client.Instance.PostAsync<bool>("report/createreport",null);
            return View("Success");
        }

        public async Task<IActionResult> ReportList()
        {
            var result = await Client.Instance.GetAsync<List<ReportDto>>("report/getreportlist");
            return View(result);
        }

        public async Task<IActionResult> Detail(string id)
        {
            var result = await Client.Instance.GetAsync<List<ReportDetailDto>>("report/getreportdetaillist",new {reportId = id});
            return View(result);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}