﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.DtoModel;
using Infrastructure.Entity;
using Infrastructure.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;

namespace BATest.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly ILogger<PersonController> _logger;
        private readonly IPersonRepository _personRepository;
        public PersonController(ILogger<PersonController> logger,IPersonRepository personRepository)
        {
            _personRepository = personRepository;
            _logger = logger;
        }

        [HttpGet("getall")]
        public async Task<IEnumerable<PersonDto>> Get()
        {
            var persons = await _personRepository.GetAllPersons();
            return persons.Select(p => new PersonDto()
            {
                PersonId = p.Uuid, 
                Name = p.Name, 
                Surname = p.Surname, 
                Company = p.Company
            });
        }

        [HttpPost("createperson")]
        public async Task<bool> CreatePerson(PersonDto personDto)
        {
            var person = new Person();
            person.CopyPropertiesFrom(personDto);
            return await _personRepository.CreatePerson(person);
        }

        [HttpDelete("deleteperson")]
        public async Task<bool> DeletePerson(string id)
        {
            return await _personRepository.DeletePerson(new Guid(id));
        }

        [HttpGet("getallcontacts")]
        public async Task<IEnumerable<ContactDto>> GetContacts(string id)
        {
            var contactList = await _personRepository.GetContacts(new Guid(id));
            return contactList.Select(s=> new ContactDto
            {
                Id = s.Uuid,
                PersonId = s.PersonId,
                TelNo = s.TelNo,
                Email = s.Email,
                Location = s.Location
            });
        }

        [HttpDelete("removecontact")]
        public async Task<bool> RemoveContact(string id,string contactId)
        {
            return await _personRepository.RemoveContact(new Guid(id),new Guid(contactId));
        }

        [HttpPost("addcontact")]
        public async Task<bool> AddContact(ContactDto contactDtp)
        {
            var contact = new Contact();
            contact.CopyPropertiesFrom(contactDtp);
            return await _personRepository.AddContact(contact);
        }
        
    }
}