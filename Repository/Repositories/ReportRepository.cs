﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure;
using Infrastructure.DtoModel;
using Infrastructure.Entity;
using Infrastructure.Enum;
using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;

namespace Repository.Repositories
{
    public class ReportRepository : IReportRepository
    {
        private readonly BaTestDbContext _dbc;

        public ReportRepository(BaTestDbContext dbc)
        {
            _dbc = dbc;
        }


        public async Task<Guid> CreateReport()
        {
            var report = new Report
            {
                ReportDate = DateTime.Now,
                ReportState = ReportState.Hazırlanıyor
            };
            await _dbc.Report.AddAsync(report);
            await _dbc.SaveChangesAsync();
            return report.Uuid;
        }

        public async Task Calculate(Guid reportId)
        {

            var reportDetailList = (from c in _dbc.Contact
                group c by c.Location
                into g
                select new ReportDetailDto
                {
                    Location = g.Key,
                    PersonCount = (from p in _dbc.Contact
                                where p.Location == g.Key
                                group p by p.PersonId into pg
                                select pg.Key).Count(),
                    TelNoCount = (from p in _dbc.Contact
                                where p.Location == g.Key && p.TelNo != null
                                select p.TelNo).Count()
                }).ToList();

            foreach (var reportDto in reportDetailList)
            {
                _dbc.ReportDetail.Add(new ReportDetail
                {
                    Location = reportDto.Location,
                    PersonCount = reportDto.PersonCount,
                    TelNoCount = reportDto.TelNoCount,
                    ReportId = reportId
                });
            }

            var report = await _dbc.Report.Where(w => w.Uuid == reportId).FirstOrDefaultAsync();
            report.ReportState = ReportState.Tamamlandı;
            _dbc.Entry(report).Property(x => x.ReportState).IsModified = true;

            await _dbc.SaveChangesAsync();
        }

        public async Task<List<Report>> GetReportList()
        {
            return await _dbc.Report.OrderByDescending(o=>o.ReportDate).ToListAsync();
        }

        public async Task<List<ReportDetail>> GetReportDetailList(Guid reportId)
        {
            return await _dbc.ReportDetail.Where(w => w.ReportId == reportId).ToListAsync();
        }
    }
}