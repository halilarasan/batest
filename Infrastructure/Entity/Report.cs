﻿using System;
using System.Collections.Generic;
using Infrastructure.Enum;

namespace Infrastructure.Entity
{
    public class Report : BaseEntity
    {
        public DateTime ReportDate { get; set; }
        public ReportState ReportState { get; set; }
        
        public ICollection<ReportDetail> ReportDetail { get; set; }
    }
}