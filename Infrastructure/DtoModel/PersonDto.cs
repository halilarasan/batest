using System;
using Infrastructure.Enum;

namespace Infrastructure.DtoModel
{
    public class PersonDto
    {
        public Guid PersonId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Company { get; set; }
    }
}