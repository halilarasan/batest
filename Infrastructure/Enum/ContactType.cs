﻿namespace Infrastructure.Enum
{
    public enum ContactType
    {
        TelNo,
        Email,
        Location
    }
}