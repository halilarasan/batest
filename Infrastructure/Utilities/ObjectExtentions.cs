using System;

namespace Infrastructure.Utilities
{
    public static class ObjectExtentions
    {
        public static void CopyPropertiesFrom(this object self, object parent)
        {
            if (parent == null) return; 
            var fromProperties = parent.GetType().GetProperties();
            var toProperties = self.GetType().GetProperties();

            foreach (var fromProperty in fromProperties)
            {
                foreach (var toProperty in toProperties)
                {
                    if (fromProperty.Name == toProperty.Name)
                    {
                        if(fromProperty.PropertyType == toProperty.PropertyType)
                        {
                            toProperty.SetValue(self, fromProperty.GetValue(parent));
                        }
                        else if(toProperty.PropertyType.IsGenericType && toProperty.PropertyType.GetGenericTypeDefinition()== typeof(Nullable<>))
                        {
                            if (fromProperty.PropertyType == Nullable.GetUnderlyingType(toProperty.PropertyType))
                            {
                                toProperty.SetValue(self, fromProperty.GetValue(parent));
                            }
                        }
                      
                        break;
                    }
                }
            }
        }
    }
}