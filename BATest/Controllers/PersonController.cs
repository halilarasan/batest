﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BATest.Models;
using BATest.Utilities;
using Infrastructure;
using Infrastructure.DtoModel;
using Infrastructure.Entity;
using Repository.Interfaces;

namespace BATest.Controllers
{
    public class PersonController : Controller
    {
        private readonly ILogger<PersonController> _logger;
        public PersonController(ILogger<PersonController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var result = await Client.Instance.GetAsync<List<PersonDto>>("person/getall");
            return View(result);
        }

        public IActionResult NewPerson()
        {
            return View();
        }

        public async Task<IActionResult> CreatePerson(PersonDto person)
        {
            var result = await Client.Instance.PostAsync<bool>("person/createperson",person);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeletePerson(string id)
        {
            var result = await Client.Instance.DeleteAsync<bool>("person/deleteperson",new {id=id});
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Contact(string id)
        {
            ViewBag.PersonId = id;
            var result = await Client.Instance.GetAsync<List<ContactDto>>("person/getallcontacts",new {id=id});
            return View(result);
        }
        
        public IActionResult NewContact(string id)
        {
            ViewBag.PersonId = id;
            return View();
        }
        
        public async Task<IActionResult> RemoveContact(string id,string contactId)
        {
            //var result = await _personRepository.RemoveContact(new Guid(id),new Guid(contactId));
            var result = await Client.Instance.DeleteAsync<bool>("person/removecontact",new {id=id,contactId=contactId});
            return RedirectToAction("Contact",new {id = id});
        }
        
        public async Task<IActionResult> AddContact(ContactDto contact)
        {
            //var result = await _personRepository.AddContact(contact);
            var result = await Client.Instance.PostAsync<bool>("person/addcontact",contact);
            return RedirectToAction("Contact",new {id = contact.PersonId});
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}