﻿using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Entity
{
    public class BaTestDbContext : DbContext
    {
        public BaTestDbContext(DbContextOptions<BaTestDbContext> options) : base(options)
        {
            
        }
        
        public DbSet<Person> Person { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Report> Report { get; set; }
        public DbSet<ReportDetail> ReportDetail { get; set; }
        
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp");
                
            modelBuilder.Entity<Person>()
                .Property(e => e.Uuid)
                .HasDefaultValueSql("uuid_generate_v4()");
            
            modelBuilder.Entity<Contact>()
                .Property(e => e.Uuid)
                .HasDefaultValueSql("uuid_generate_v4()");
            
            modelBuilder.Entity<Report>()
                .Property(e => e.Uuid)
                .HasDefaultValueSql("uuid_generate_v4()");
            
            base.OnModelCreating(modelBuilder);
        }
    }
}